<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Юглесэнерго-Плюс</title>
</head>
<body>
<?php require "header.php"; ?>

<div class="container">
    <h1>УСЛУГИ</h1>
        <ul>
            <li>Лабораторно-технические измерения в области экоаналитического и технологического контроля.</li>
            <li>Проведение измерений и анализов
                <ul>
                    <li>источников выброса загрязняющих веществ (ЗВ) в атмосферный воздух;</li>
                    <li>в контрольных точках границы санитарно-защитной зоны;</li>
                    <li>в контрольных точках жилой зоны;</li>
                    <li>в рабочей зоне.</li>
                </ul>
            </li>
            <li>Проведение инвентаризации источников промышленных выбросов загрязняющих веществ (ЗВ) в атмосферный воздух.</li>
            <li>Разработка проектов
                <ul>
                    <li>нормативов образования отходов и лимитов на их размещение (ПНООЛР);</li>
                    <li>нормативов предельнодопустимых выбросов (ПДВ) загрязняющих веществ (ЗВ) в атмосферный воздух;</li>
                    <li>организации санитарно-защитной зоны.</li>
                </ul>
            </li>
            <li>Паспортизация пылегазоочистных установок.</li>
            <li>Расчёт платы за негативное воздействие на окружающую природную среду.</li>
            <li>Составление отчетности об образовании, использовании, обезвреживании и размещении
                отходов (за исключением статистической отчетности) для субъектов малого и среднего предпринимательства.</li>
            <li>Составление отчета по форме федерального статистического наблюдения N 2-ТП (отходы) "Сведения об образовании,
                использовании, обезвреживании, транспортировании и размещении отходов производства и потребления".</li>
            <li>Оценка воздействия намечаемой хозяйственной и
                иной деятельности на окружающую природную среду (ОВОС).</li>
            <li>Подготовка материалов экологического обоснования деятельности по обращению с опасными отходами.</li>
            <li>Разработка программы производственного экологического контроля.</li>
            <li>Оформление технических отчетов о неизменности производственного процесса,
                используемого сырья и образующихся отходов за отчетный период.</li>
            <li>Оформление отчета об организации и о результатах осуществления производственного экологического контроля.</li>
            <li>Согласование разработанной документации в государственных контролирующих органах.</li>
            <li>Оказание консультационных услуг.</li>
        </ul>
</div>


<?php require "footer.php"; ?>

</body>
</html>