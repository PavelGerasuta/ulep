<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Юглесэнерго-Плюс</title>
</head>
<body>
<?php require "header.php"; ?>

<div class="container">
    <h1>О НАС</h1>
    <p>Общество с ограниченной ответственностью «Юглесэнерго-Плюс» создано
        в 2002 году на базе существующей с 1984 года специализированной лаборатории по охране
        окружающей среды ЗАО «Юглесэнерго». Основным видом нашей деятельности,
        подтверждённым государственной лицензией, является оказание услуг предприятиям по соблюдению
        природоохранного законодательства.
        Руководствуясь в своей деятельности нормативно-методической базой
        Министерства природных ресурсов и экологии Российской Федерации,
        Федеральной службы по надзору в сфере природопользования (Росприроднадзора),
        Федеральной служба по надзору в сфере защиты прав потребителей и
        благополучия человека (Роспотребнадзора), мы имеем многолетний опыт работы в
        области охраны окружающей природной среды.
        Область аккредитации нашей аналитической лаборатории, оснащённой новейшими приборами и
        оборудованием, позволяет нам осуществлять аналитический контроль
        за загрязнением окружающей природной среды во всех экосферах.
    </p>
    <div class="image">
        <img src="img/lich.jpg" alt="">
        <img src="img/cerh.jpg" alt="">
        <img src="img/atth.jpg" alt="">
    </div>

    <p>
        Знание специфики экологической ситуации в Краснодарском крае,
        богатый опыт работы с предприятиями-природопользователями,
        высокий уровень выполнения договорных обязательств
        позволяют нам обеспечить своевременное и качественное выполнение работ природоохранного назначения.
    </p>
</div>

<?php require "footer.php"; ?>
</body>
</html>